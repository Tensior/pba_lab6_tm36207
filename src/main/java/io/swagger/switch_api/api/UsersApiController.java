package io.swagger.switch_api.api;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.swagger.Swagger2SpringBoot;
import io.swagger.switch_api.model.*;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-07T15:16:08.865+02:00")

@Controller
public class UsersApiController implements UsersApi {

    @Autowired
    UserRepository repository;

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

//    private Algorithm algorithm = Algorithm.HMAC256("123456");

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<UserResponse> createUser(@ApiParam(value = "X-HMAC-SIGNATURE" ,required=true) @RequestHeader(value="X-HMAC-SIGNATURE", required=true) String X_HMAC_SIGNATURE, @ApiParam(value = "User object that has to be added" ,required=true )  @Valid @RequestBody CreateRequest body) {
//        System.out.println(X_HMAC_SIGNATURE);
        System.out.println(Hmac256());
        String accept = request.getHeader("Accept");
        User user = body.getUser();
        UUID id = UUID.randomUUID();
//        user.setId(id);
        String json = new Gson().toJson(user);
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        String stringHmac = null;
        try {
            String SecretKey = "123456";
            byte[] hmacSha256 = null;
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKey secretKeySpec = new SecretKeySpec(SecretKey.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(json.getBytes(StandardCharsets.UTF_8));
//            System.out.println(json);
//            System.out.println(String.format("Hex: %032x", new BigInteger(1, hmacSha256)));
            stringHmac = String.format("%032x", new BigInteger(1, hmacSha256));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
//        System.out.println(stringHmac);
        if (accept != null && accept.contains("application/json") && stringHmac.equals(X_HMAC_SIGNATURE)) {
            try {
                repository.save(user);
                return new ResponseEntity<UserResponse>(objectMapper.readValue(
                        "{  \"responseHeader\" : {" +
                                "    \"sendDate\" : \""+ time + "\"," +
                                "    \"requestId\" :  \"" + id +"\"  },  " +
                                "\"user\" : "+ json + " }", UserResponse.class), HttpStatus.CREATED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteUser(@ApiParam(value = "",required=true) @PathVariable("id") UUID id) {
        String accept = request.getHeader("Accept");
        UUID requestId = UUID.randomUUID();
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        User user = repository.getOne(id);
        if (user != null) {
            repository.delete(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<UserListResponse> getAllUsers() {
        String accept = request.getHeader("Accept");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        String json = new Gson().toJson(repository.findAll());
        UUID requestId = UUID.randomUUID();


        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserListResponse>(objectMapper.readValue(
                        "{ \"usersList\" : "+json+",  " +
                                "\"responseHeader\" : {    \"sendDate\" : \""+time+"" + "\"," +
                                " \"requestId\" : \""+requestId+"\" }}", UserListResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserListResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserListResponse>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "",required=true) @PathVariable("id") UUID id) {
        String accept = request.getHeader("Accept");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        String json = new Gson().toJson(repository.getOne(id));
        UUID requestId = UUID.randomUUID();

        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserResponse>(objectMapper.readValue(
                        "{   \"responseHeader\" : {    \"sendDate\" : \""+time+"\"," +
                                "    \"requestId\" : \""+requestId.toString()+"\"  }," +
                                " \"user\" : "+json+"}", UserResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<UserResponse> updateUser(@ApiParam(value = "X-JWS-SIGNATURE" ,required=true) @RequestHeader(value="X-JWS-SIGNATURE", required=true) String X_JWS_SIGNATURE, @ApiParam(value = "",required=true) @PathVariable("id") UUID id,@ApiParam(value = "" ,required=true )  @Valid @RequestBody UpdateRequest body) {
        String accept = request.getHeader("Accept");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        UUID requestId = UUID.randomUUID();
        User newUser = body.getUser();
        User user = repository.getOne(id);
        String json = new Gson().toJson(newUser);
        try {
//            String key = "123456";
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(Swagger2SpringBoot.key).parseClaimsJws(X_JWS_SIGNATURE);
            String parsedClaims = claimsJws.getBody().toString().replace("=", ":");
            String parsedJson = json.replace("\"", "").replace(",", ", ");
//            System.out.println(parsedClaims);
//            System.out.println(parsedJson);

            if (accept != null && accept.contains("application/json") && parsedClaims.equals(parsedJson)) {
                if(user != null) {
                    try {
                        repository.save(newUser);
                        return new ResponseEntity<UserResponse>(objectMapper.readValue(
                                "{  \"responseHeader\" : {    \"sendDate\" : \""+time+"\"," +
                                        "    \"requestId\" : \""+requestId+"\"  }," +
                                        "  \"user\" :"+json+"}", UserResponse.class), HttpStatus.OK);
                    } catch (IOException e) {
                        log.error("Couldn't serialize response for content type application/json", e);
                        return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
            }
        } catch (JwtException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }

    public String Hmac256() {
//        Algorithm algorithm = Algorithm.HMAC256("secret");
        Map<String,Object> HeaderMap = new LinkedHashMap<>();
        HeaderMap.put("alg", "HS256");
        HeaderMap.put("typ", "JWT");

        Map<String,Object> ClaimMap = new LinkedHashMap<>();
        ClaimMap.put("id", "123e4567-e89b-12d3-a456-426614174022");
        ClaimMap.put("name", "Mariusz");
        ClaimMap.put("surname", "Musk");
        ClaimMap.put("age", 22);
        ClaimMap.put("personalId", "12378945612");
        ClaimMap.put("citizenship", "UK");
        ClaimMap.put("email", "elonmusk@gmail.com");

        String token = Jwts.builder()
                .setHeader(HeaderMap)
                .setClaims(ClaimMap)
                .signWith(Swagger2SpringBoot.key)
                .compact();
        return token;
    }
}
